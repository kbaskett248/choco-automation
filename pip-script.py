import os
import subprocess
import sys

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
package_list = os.path.join(SCRIPT_DIR, 'python-pip-list.txt')


def run_pip(*args, on_success='', on_failure=''):
    cmd_args = [sys.executable, '-m', 'pip']
    cmd_args.extend(args)
    try:
        output = subprocess.run(cmd_args, check=True, capture_output=True)
    except subprocess.CalledProcessError:
        if on_failure:
            print(on_failure)
        return None
    else:
        if on_success:
            print(on_success)
        return output.stdout.decode('utf-8')


def install(package):
    print('Installing {0}'.format(package))
    return run_pip('install', package, on_success='{0} installed'.format(package), on_failure='{0} install failed'.format(package))


def upgrade(package):
    print('Upgrading {0}'.format(package))
    return run_pip('install', '--upgrade', package, on_success='{0} upgraded'.format(package), on_failure='{0} upgrade failed'.format(package))


upgrade('pip')

if not os.path.exists(package_list):
    print('python-pip-list.txt does not exist. Exiting.')
    sys.exit(0)

packages = run_pip('list', '--not-required', '--format', 'freeze')
installed_packages = {line.split('==')[0] for line in packages.split('\n\r')}

with open(package_list, 'r') as package_file:
    for package in package_file:
        package = package.strip()
        if package in installed_packages:
            upgrade(package)
        else:
            install(package)

@echo off

:check-install-mode
if "%1"=="" (
    echo Failure: Must specify either "work" or "home"
    goto END
) else if "%1" == "home" (
    echo Running in home mode.
) else if "%1" == "work" (
    echo Running in work mode.
) else (
    echo Failure: Must specify either "work" or "home"
    goto END
)


:check-admin-rights
net session >nul 2>&1
if %errorLevel% == 0 (
    echo Success: Administrative permissions confirmed.    
) else (
    echo Failure: Current permissions inadequate. Admin rights needed for Choco.
    goto END
)

:test-choco-install
WHERE choco
IF %ERRORLEVEL% NEQ 0 (
	ECHO choco install wasn't found ... intalling:
	@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

)

CALL %0\..\choco-script.cmd %1

:test-pip-install
WHERE python
IF %ERRORLEVEL% NEQ 0 (
	ECHO Python wasn't installed; skipping python package installation
) else (
    CALL python %0\..\pip-script.py %1
)

:END
pause







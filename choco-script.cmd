@echo off

:choco-upgrade
choco upgrade -y chocolatey
choco upgrade -y all

:choco-list-outdated
choco outdated

:choco-list-to-file
choco "list" "-l" "--id-only" > %~dp0temp

:combine-lists
copy /B %~dp0common-list.txt + %~dp0%1-list.txt %~dp0soft-list.txt

:choco-install-list
FOR /F %%L IN (%~dp0soft-list.txt) DO (
	SET "found="	
	FOR /F %%T in (%~dp0temp) DO (		
		IF "%%L"=="%%T" (SET "found=1")					
	)

	IF NOT DEFINED found (
		ECHO
		ECHO ********************************************
		ECHO Found new software to install: %%L
		ECHO ********************************************
		ECHO
		choco install -y %%L

		FOR /F %%T in (%~dp0pin-list.txt) DO (		
			IF "%%L"=="%%T" (
				ECHO
				ECHO ********************************************
				ECHO Pinning software version to prevent updates: %%L
				ECHO ********************************************
				ECHO
				choco pin -n=%%L
			)					
		)
	)
)

:delete-temp
del %~dp0temp
del %~dp0soft-list.txt



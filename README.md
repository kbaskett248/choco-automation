README.md

# README

Installs a list of preferred windows software via chocolatey. A good way to start from scrach with a bunch of sofware and keep it updated.

## Installations steps

- **Download** project as Zip

    ![Download Zip](download-zip.png)


- **Extract** in local filesystem
- **Edit** `common-list.txt`, `home-list.txt` and `work-list.txt` and add or delete software packages. [package list](https://chocolatey.org/packages).
- **Add** any auto-updating packages to `pin-list.txt`, like Sublime Text.
- **Execute** `setup.cmd <mode>` as _admin_, where `mode` is "home" or "work".

> Note 1: It's possible to put a `choco-script.cmd` shortcut in your desktop and use image file `Project-choco.ico` file as the shorcut icon. Read 
>   - https://www.isumsoft.com/windows-10/how-to-change-icon-for-desktop-shortcut.html

> Note 2: As a shorcut you can set properties in order to execute always with admin rights. Read:
>   - https://www.groovypost.com/howto/make-windows-10-apps-always-run-with-administrator-privileges/

## Features
- Checks admins rights (`choco` needs admin rights for install commands).
- Installs chocolatey if not already installed.
- Upgrades chocolatey.
- Upgrades software already installed.
- Installs other software if not already installed.
- Pins software that auto-updates.

## Software to install

The list of packages you want to install is split between `common-list.txt`, `home-list.txt` and `work-list.txt`. `common-list.txt` is installed in both modes. Change, add, delete whatever package name you want:
> See choco [package list](https://chocolatey.org/packages)

>Example:

```
7zip
autohotkey 
git
greenshot
sublimetext3 
tailblazer 
vlc
```

The list of packages that auto-update are listed in the file `pin-list.txt`:

>Example

```
dropbox
sublimetext3
```
